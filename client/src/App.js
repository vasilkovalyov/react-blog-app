import React from 'react'
import './assets/styles/main.less'

import {
    BrowserRouter as Router,
    Switch
} from "react-router-dom";
// pages
import { Home, Blog, Login, Register } from './views/main/main'
// layouts
import MainLayout from './components/layouts/MainLayout.jsx'
import AppRoute from './views/AppRoute.jsx'

const App = () => {
    return (
        <div id="wrapper">
            <Router>
                <Switch>
                    <AppRoute exact path="/" component={Home} layout={MainLayout} />
                    <AppRoute exact path="/blog" component={Blog} layout={MainLayout} />
                    <AppRoute exact path="/login" component={Login} layout={MainLayout} />
                    <AppRoute exact path="/register" component={Register} layout={MainLayout} />
                </Switch>
            </Router>
        </div>
    )
}

export default App