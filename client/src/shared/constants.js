export const BREAKPOINTS = {
    widescreen: 1366,
    desktop: 1024,
    tablet: 768,
    mobile: 480,
    smallMobile: 320
}
