import React from 'react'
import { Link } from 'react-router-dom'
import Divider from 'antd/es/divider/index';

import Image from '../common/Image.jsx'

import { Menu } from 'antd'
import { Row, Col } from 'antd';

const publicNavigation = [
    {
        Path: '/',
        Name: 'Home'
    },
    
    {
        Path: '/blog',
        Name: 'Blog'
    }
]

const getNavigation = (HeaderNavigation) => {
    return (
        <Menu className="d-flex d-flex--ended d-flex--center header__menu" theme="light">
            {
                HeaderNavigation && HeaderNavigation.map((item, index) => {
                    return (
                        <Menu.Item key={index}>
                            <Link to={item.Path}>{item.Name}</Link>
                        </Menu.Item>
                    )
                })
            }
        </Menu>
    )
}

const HeaderPublic = () => {
    const HeaderLogo = {
        Name: 'Blog App Logo',
        Url: 'https://i.postimg.cc/ZRv35qXj/logo.png'
    }
    return <header className="header">
        <div className="container">
            <Row justify="space-between" align="middle">
                <Col xs={4}>
                    <Link to="/" className="header__logo-link d-inline-block">
                        <Image ClassName="header__logo" Image={HeaderLogo} />
                    </Link>
                </Col>
                <Col md={12}>
                    <nav className="header__navigation">
                        { getNavigation(publicNavigation) }
                        <Divider className="header__devider" type="vertical" />
                        <Menu className="header__menu">
                            <Menu.Item>
                                <Link to="/login">Login</Link>
                            </Menu.Item>
                        </Menu>
                    </nav>
                </Col>
            </Row>
        </div>
    </header>
}

export default HeaderPublic