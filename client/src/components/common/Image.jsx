import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import imageModel from '../../models/Image.js'

const Image = ({Image, ClassName, Ratio}) => {
    return (
        <div className={cn(`image ${ClassName || ''}`, {
            'image--wide': Ratio ==='wide',
            'image--square': Ratio === 'square'
        })}>
            <figure>
                <picture>
                    <img src={Image.Url} alt={Image.Name} />
                </picture>
            </figure>
        </div>
    )
}

Image.propTypes = {
    Image: PropTypes.shape(imageModel),
    ClassName: PropTypes.string,
    Ratio: PropTypes.oneOf(["squared", "wide"]),
}

export default Image
